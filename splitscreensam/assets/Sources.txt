Assets-zombie stuff: Top Down Shooter: Main Characters by Free Game Assets (GUI, Sprite, Tilesets) https://free-game-assets.itch.io/top-down-shooter-main-characters
	used https://codeshack.io/images-sprite-sheet-generator/ to make the sprite sheets
Background image: Zombie_background: https://www.freepik.com/premium-vector/abstract-biohazard-symbol-dark-red-background_1567421.htm#page=1&query=zombie%20background&position=2

Sound-effects: Gun cocking fast sound = http://soundbible.com/1988-Gun-Cocking-Fast.html
		Shot gun reload pump sound = http://soundbible.com/1959-Shotgun-Reload-Pump.html
		
Sound-Background: Purple planet music predator https://www.purple-planet.com/tense
