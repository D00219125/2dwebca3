/**
 * Moves the parent sprite based on keyboard input and detect collisions against platforms, pickups etc
 * @author niall mcguinness
 * @version 1.0
 * @class SamPlayerBehavior
 */
class SamPlayerBehavior extends Behavior{
  //#region Static Fields
  //#endregion

  //#region Fields
  animResponse = [
    {
      //animationType: 1,
      walkTake: "walk_with_bat",
      idleTake: "idle_with_bat"
    },
    {
      //animationType: 2,
      walkTake: "walk_with_pistol",
      idleTake: "idle_with_pistol"
    },
    {
      //animationType: 3,
      walkTake: "walk_with_rifle",
      idleTake: "idle_with_rifle"
    },
    {
      //animationType: 4,
      walkTake: "walk_with_flamethrower",
      idleTake: "idle_with_flamethrower"
    }
  ];
  //animIndex = 0;
  //#endregion

  //#region Properties
  //#endregion

  constructor(
    keyboardManager,
    objectManager,
    moveKeys,
    originalDirection,
    moveSpeed = 2,
    rotateSpeed = 0.04, 
    //added this
    animIndex = 0
  ) {
    super();
    this.keyboardManager = keyboardManager;
    this.objectManager = objectManager;

    this.moveKeys = moveKeys;
    this.originalDirection = originalDirection.Clone(); //direction we were facing at game start
    this.lookDirection = originalDirection.Clone(); //direction after we turn

    this.moveSpeed = moveSpeed;
    this.rotateSpeed = rotateSpeed;
    
    //new stuff
    this.invulnerabilityTimer = new Stopwatch();
    this.invulnerabilityTimer.Reset();
    this.timeSinceLastInMs = 0;
    this.animIndex = animIndex;
  }

  //#region Your Game Specific Methods - add code for more CD/CR or input handling
  HandleInput(gameTime, parent, animIndex) {
    this.HandleMove(gameTime, parent, animIndex);
  }

  HandleMove(gameTime, parent, animIndex) {
    //ad in &&'s for if the player has a bat, gun etc
    //up/down
    if (this.keyboardManager.IsKeyDown(this.moveKeys[0])) {
      //rotate the look direction used to move the sprite when forward/backward keys are pressed
      this.lookDirection = Vector2.Normalize(Vector2.Transform(this.originalDirection, Matrix.CreateRotationZ(parent.Transform2D.RotationInRadians))); //change the animation to something else (e.g. idle or use your own animation here)

      //move forward using the look direction
      parent.Body.AddVelocity(Vector2.MultiplyScalar(this.lookDirection, gameTime.ElapsedTimeInMs * this.moveSpeed));

      //change the animation to something else (e.g. idle or use your own animation here)
      // console.log(animIndex);

      // console.log(this.animResponse);

      // console.log(this.animResponse[animIndex]);
      parent.Artist.SetTake(this.animResponse[animIndex].walkTake);           //this is what is supposed to switch it but it's undefined
    } else if (this.keyboardManager.IsKeyDown(this.moveKeys[1])) {
      //rotate the look direction used to move the sprite when forward/backward keys are pressed
      this.lookDirection = Vector2.Normalize(Vector2.Transform(this.originalDirection, Matrix.CreateRotationZ(parent.Transform2D.RotationInRadians))); //change the animation to something else (e.g. idle or use your own animation here)

      //move backward using the look direction
      parent.Body.AddVelocity(Vector2.MultiplyScalar(this.lookDirection, -gameTime.ElapsedTimeInMs * this.moveSpeed));

      //change the animation to something else (e.g. idle or use your own animation here)
      parent.Artist.SetTake(this.animResponse[animIndex].idleTake);
    }

    //turn left/right
    if (this.keyboardManager.IsKeyDown(this.moveKeys[2])) {
      //rotate the drawn sprite
      parent.Transform2D.RotateBy(GDMath.ToRadians(-4));

      //change the animation to something else (e.g. idle or use your own animation here)
      parent.Artist.SetTake(this.animResponse[animIndex].idleTake);
    } else if (this.keyboardManager.IsKeyDown(this.moveKeys[3])) {
      //rotate the drawn sprite
      parent.Transform2D.RotateBy(GDMath.ToRadians(4));

      //change the animation to something else (e.g. idle or use your own animation here)
      parent.Artist.SetTake(this.animResponse[animIndex].idleTake);
    }
  }

  CheckCollisions(parent, gameTime) {

    this.HandleArchitectureCollision(parent);

    this.HandleEnemyCollision(parent);

    this.HandlePickupCollision(parent, gameTime);
  }

  HandlePickupCollision(parent, gameTime) {
    let sprites = this.objectManager.Get(ActorType.Pickup);

    for (let i = 0; i < sprites.length; i++) {
      let sprite = sprites[i];

      //we can use simple collision check here (i.e. Intersects) because dont need to think was it top, bottom, left, or right
      if (Collision.Intersects(parent, sprite)) {
        //your code - play sound, remove enemy, add health e.g. you could write code like this...
        //I've spent too long working on this So I'm just going to hard code it, also I didn't think about switches until after 90% completion
        console.log(sprite.id);
        if(sprite.id == "zlime")
        {
          NotificationCenter.Notify(
            new Notification(NotificationType.Sound, 
              NotificationAction.Play, [
              "zlime_sound"]));

        }
        if(sprite.id == "block[1,8]")
        {
          console.log("end game")
          NotificationCenter.Notify(new Notification (NotificationType.Menu, NotificationAction.Win));

        }
        if(sprite.id == "block[31,1]")
        {
          this.animIndex = 1;
          NotificationCenter.Notify(
            new Notification(NotificationType.Sound, 
              NotificationAction.Play, [
              "pistol_pickup"]));
              this.HandleMove(gameTime, parent, this.animIndex);
              console.log(this.animIndex);
        }
        if(sprite.id == "block[24,1]")
        {
          this.animIndex = 1;
          NotificationCenter.Notify(
            new Notification(NotificationType.Sound, 
              NotificationAction.Play, [
              "pistol_pickup"]));
              this.HandleMove(gameTime, parent, this.animIndex);
              console.log(this.animIndex);
        }
        if(sprite.id == "block[24,17]")
        {
          this.animIndex = 2;
          NotificationCenter.Notify(
            new Notification(NotificationType.Sound, 
              NotificationAction.Play, [
              "rifle_pickup"]));
              this.HandleMove(gameTime, parent, this.animIndex);
              console.log(this.animIndex);
        }
        if(sprite.id == "block[22,8]")
        {
          this.animIndex = 1;
          NotificationCenter.Notify(
            new Notification(NotificationType.Sound, 
              NotificationAction.Play, [
              "rifle_pickup"]));
              this.HandleMove(gameTime, parent, this.animIndex);
              console.log(this.animIndex);
        }
        if(sprite.id == "block[20,6]")
        {
          this.animIndex = 3;
          NotificationCenter.Notify(
            new Notification(NotificationType.Sound, 
              NotificationAction.Play, [
              "flamethrower_pickup"]));
              this.HandleMove(gameTime, parent, this.animIndex);
              console.log(this.animIndex);
        }
        if(sprite.id == "block[21,6]")
        {
          this.animIndex = 3;
          NotificationCenter.Notify(
            new Notification(NotificationType.Sound, 
              NotificationAction.Play, [
              "flamethrower_pickup"]));
              this.HandleMove(gameTime, parent, this.animIndex);
              console.log(this.animIndex);
        }
        if(sprite.id == "block[9,1]")
        {
          this.animIndex = 2;
          NotificationCenter.Notify(
            new Notification(NotificationType.Sound, 
              NotificationAction.Play, [
              "rifle_pickup"]));
              this.HandleMove(gameTime, parent, this.animIndex);
              console.log(this.animIndex);
        }
        if(sprite.id == "block[9,13]")
        {
          this.animIndex = 2;
          NotificationCenter.Notify(
            new Notification(NotificationType.Sound, 
              NotificationAction.Play, [
              "rifle_pickup"]));
              this.HandleMove(gameTime, parent, this.animIndex);
              console.log(this.animIndex);
        }


        //notify MyGameStateManager (which listens for game state changes) that health has increased and to play animation at the position of the removed sprite
        NotificationCenter.Notify(
          new Notification(
            NotificationType.GameState,   //notifies the MyGameStateManager
            NotificationAction.Pickup,    //indicates the type of game state event
            //pass value and a reference to the sprite we picked-up - see MyGameStateManager
            [
              5,                          //value of the pickup
              "coin_pickup_decorator",    //id of the animation decorator to play (e.g. starburst) which must be already stored by MyGameStateManager
              parent,                     //reference to player that picked it up
              sprite                      //reference to the pickup sprite
            ])); 

        //play sound
        NotificationCenter.Notify(
          new Notification(NotificationType.Sound, 
            NotificationAction.Play, [
            "pistol_pickup"])); //id of the audio cue to play - see MyConstants::audioCueArray

        //remove coin
        NotificationCenter.Notify(
          new Notification(
            NotificationType.Sprite,
            NotificationAction.RemoveFirst,
            [sprite])); //reference to the pickup sprite
      }
    }
  }

  HandleEnemyCollision(parent) {

    let sprites = this.objectManager.Get(ActorType.Player);

    for (let i = 0; i < sprites.length; i++) {
      let sprite = sprites[i];

      if (Collision.Intersects(parent, sprite)) {
        //stop the player from moving otherwise s/he will move through the enemy!
        parent.Body.SetVelocityX(0);
        parent.Body.SetVelocityY(0);

        //play sound
        NotificationCenter.Notify(
          new Notification(NotificationType.Sound, NotificationAction.Play, [
            "coin_pickup"
          ]));

        //remove health
        NotificationCenter.Notify(
          new Notification(
            NotificationType.GameState,
            NotificationAction.Health,
            [-10]));
      }
    }
  }

  //#endregion

  //#region Core Methods - doesnt need to change
  Execute(gameTime, parent) {
    this.HandleInput(gameTime, parent, this.animIndex);
    this.ApplyForces(parent);
    this.CheckCollisions(parent, gameTime);
    this.ApplyInput(parent);
  }

  ApplyForces(parent) {
    //notice we need to slow body in X and Y and we dont ApplyGravity() in a top-down game
    parent.Body.ApplyFrictionX();
    parent.Body.ApplyFrictionY();
  }

  HandleArchitectureCollision(parent) {
    let sprites = this.objectManager.Get(ActorType.Architecture);

    for (let i = 0; i < sprites.length; i++) {
      let sprite = sprites[i];
      let collisionLocationType = Collision.GetIntersectsLocation(parent, sprite);

      //the code below fixes a bug which caused sprites to stick inside an object
      if (collisionLocationType === CollisionLocationType.Left) {
        if (parent.Body.velocityX <= 0)
          parent.Body.SetVelocityX(0);
      } else if (collisionLocationType === CollisionLocationType.Right) {
        if (parent.Body.velocityX >= 0)
          parent.Body.SetVelocityX(0);
      }
      //the code below fixes a bug which caused sprites to stick inside an object
      if (collisionLocationType === CollisionLocationType.Top) {
        if (parent.Body.velocityY <= 0)
          parent.Body.SetVelocityY(0);
      } else if (collisionLocationType === CollisionLocationType.Bottom) {
        if (parent.Body.velocityY >= 0)
          parent.Body.SetVelocityY(0);
      }

    }
  }

  HandleShoot(gameTime, parent)
  {
    this.timeSinceLastInMs = 0
    let bulletClone;
  }

  ApplyInput(parent) {
    //if we have small left over values then zero
    if (Math.abs(parent.Body.velocityX) <= Body.MIN_SPEED)
      parent.Body.velocityX = 0;
    if (Math.abs(parent.Body.velocityY) <= Body.MIN_SPEED)
      parent.Body.velocityY = 0;

    //apply velocity to (x,y) of the parent's translation
    parent.Transform2D.TranslateBy(new Vector2(parent.Body.velocityX, parent.Body.velocityY));
  }
  //#endregion

  //#region Common Methods - Equals, ToString, Clone
  Equals(other) {
    //to do...
    throw "Not Yet Implemented";
  }

  ToString() {
    //to do...
    throw "Not Yet Implemented";
  }

  Clone() {
    //to do...
    throw "Not Yet Implemented";
  }
  //#endregion
}

/**
 * Moves the parent sprite based on keyboard input and detect collisions against platforms, pickups etc
 * @author niall mcguinness
 * @version 1.0
 * @class SamPlayerBehavior
 
class SamPlayerBehavior extends Behavior{
  //#region Static Fields
  //#endregion

  //#region Fields
  //#endregion

  //#region Properties
  //#endregion

  constructor(
    keyboardManager,
    objectManager,
    moveKeys,
    originalDirection,
    moveSpeed = 2,
    rotateSpeed = 0.04
  ) {
    super();
    this.keyboardManager = keyboardManager;
    this.objectManager = objectManager;

    this.moveKeys = moveKeys;
    this.originalDirection = originalDirection.Clone(); //direction we were facing at game start
    this.lookDirection = originalDirection.Clone(); //direction after we turn

    this.moveSpeed = moveSpeed;
    this.rotateSpeed = rotateSpeed;
  }

  //#region Your Game Specific Methods - add code for more CD/CR or input handling
  HandleInput(gameTime, parent) {
    this.HandleMove(gameTime, parent,"walk_with_bat");
  }

  HandleMove(gameTime, parent, take) {
    //ad in &&'s for if the player has a bat, gun etc
    //up/down

      if (this.keyboardManager.IsKeyDown(this.moveKeys[0])) {
      //rotate the look direction used to move the sprite when forward/backward keys are pressed
      this.lookDirection = Vector2.Normalize(Vector2.Transform(this.originalDirection, Matrix.CreateRotationZ(parent.Transform2D.RotationInRadians))); //change the animation to something else (e.g. idle or use your own animation here)

      //move forward using the look direction
      parent.Body.AddVelocity(Vector2.MultiplyScalar(this.lookDirection, gameTime.ElapsedTimeInMs * this.moveSpeed));

      //change the animation to something else (e.g. idle or use your own animation here)
      parent.Artist.SetTake(take);
    } 
    else if (this.keyboardManager.IsKeyDown(this.moveKeys[1])) {
      //rotate the look direction used to move the sprite when forward/backward keys are pressed
      this.lookDirection = Vector2.Normalize(Vector2.Transform(this.originalDirection, Matrix.CreateRotationZ(parent.Transform2D.RotationInRadians))); //change the animation to something else (e.g. idle or use your own animation here)

      //move backward using the look direction
      parent.Body.AddVelocity(Vector2.MultiplyScalar(this.lookDirection, -gameTime.ElapsedTimeInMs * this.moveSpeed));

      //change the animation to something else (e.g. idle or use your own animation here)
      parent.Artist.SetTake(take);
    }

    //turn left/right
    if (this.keyboardManager.IsKeyDown(this.moveKeys[2])) {
      //rotate the drawn sprite
      parent.Transform2D.RotateBy(GDMath.ToRadians(-4));

      //change the animation to something else (e.g. idle or use your own animation here)
      parent.Artist.SetTake(take);
    } 
    else if (this.keyboardManager.IsKeyDown(this.moveKeys[3])) {
      //rotate the drawn sprite
      parent.Transform2D.RotateBy(GDMath.ToRadians(4));

      //change the animation to something else (e.g. idle or use your own animation here)
      parent.Artist.SetTake(take);
    }
  }

  CheckCollisions(parent, gameTime) {

    this.HandleArchitectureCollision(parent);

    this.HandleEnemyCollision(parent);

    this.HandlePickupCollision(parent, gameTime);
  }

  HandlePickupCollision(parent, gameTime) {
    let sprites = this.objectManager.Get(ActorType.Pickup);
    //console.log(sprites[1]);
    for (let i = 0; i < sprites.length; i++) {
      let sprite = sprites[i];

      //we can use simple collision check here (i.e. Intersects) because dont need to think was it top, bottom, left, or right
      if (Collision.Intersects(parent, sprite)) {
        //your code - play sound, remove enemy, add health e.g. you could write code like this...
        
        //notify MyGameStateManager (which listens for game state changes) that health has increased and to play animation at the position of the removed sprite
        NotificationCenter.Notify(
          new Notification(
            NotificationType.GameState,   //notifies the MyGameStateManager
            NotificationAction.Pickup,    //indicates the type of game state event
            //pass value and a reference to the sprite we picked-up - see MyGameStateManager
            [
              5,                          //value of the pickup
              "coin_pickup_decorator",    //id of the animation decorator to play (e.g. starburst) which must be already stored by MyGameStateManager
              parent,                     //reference to player that picked it up
              sprite                      //reference to the pickup sprite
            ])); 

        //play sound
        NotificationCenter.Notify(
          new Notification(NotificationType.Sound, 
            NotificationAction.Play, [
            "pistol_pickup"])); //id of the audio cue to play - see MyConstants::audioCueArray

        //remove coin
        NotificationCenter.Notify(
          new Notification(
            NotificationType.Sprite,
            NotificationAction.RemoveFirst,
            [sprite])); //reference to the pickup sprite
        //change sprite
        this.HandleMove(gameTime, parent,"walk_with_pistol");
      }
    }
  }

  HandleEnemyCollision(parent) {

    let sprites = this.objectManager.Get(ActorType.Player);

    for (let i = 0; i < sprites.length; i++) {
      let sprite = sprites[i];

      if (Collision.Intersects(parent, sprite)) {
        //stop the player from moving otherwise s/he will move through the enemy!
        parent.Body.SetVelocityX(0);
        parent.Body.SetVelocityY(0);

        //play sound
        NotificationCenter.Notify(
          new Notification(NotificationType.Sound, NotificationAction.Play, [
            "coin_pickup"
          ]));

        //remove health
        NotificationCenter.Notify(
          new Notification(
            NotificationType.GameState,
            NotificationAction.Health,
            [-10]));
      }
    }
  }

  //#endregion

  //#region Core Methods - doesnt need to change
  Execute(gameTime, parent) {
    this.HandleInput(gameTime, parent);
    this.ApplyForces(parent);
    this.CheckCollisions(parent);
    this.ApplyInput(parent);
  }

  ApplyForces(parent) {
    //notice we need to slow body in X and Y and we dont ApplyGravity() in a top-down game
    parent.Body.ApplyFrictionX();
    parent.Body.ApplyFrictionY();
  }

  HandleArchitectureCollision(parent) {
    let sprites = this.objectManager.Get(ActorType.Architecture);

    for (let i = 0; i < sprites.length; i++) {
      let sprite = sprites[i];
      let collisionLocationType = Collision.GetIntersectsLocation(parent, sprite);

      //the code below fixes a bug which caused sprites to stick inside an object
      if (collisionLocationType === CollisionLocationType.Left) {
        if (parent.Body.velocityX <= 0)
          parent.Body.SetVelocityX(0);
      } else if (collisionLocationType === CollisionLocationType.Right) {
        if (parent.Body.velocityX >= 0)
          parent.Body.SetVelocityX(0);
      }
      //the code below fixes a bug which caused sprites to stick inside an object
      if (collisionLocationType === CollisionLocationType.Top) {
        if (parent.Body.velocityY <= 0)
          parent.Body.SetVelocityY(0);
      } else if (collisionLocationType === CollisionLocationType.Bottom) {
        if (parent.Body.velocityY >= 0)
          parent.Body.SetVelocityY(0);
      }

    }
  }


  ApplyInput(parent) {
    //if we have small left over values then zero
    if (Math.abs(parent.Body.velocityX) <= Body.MIN_SPEED)
      parent.Body.velocityX = 0;
    if (Math.abs(parent.Body.velocityY) <= Body.MIN_SPEED)
      parent.Body.velocityY = 0;

    //apply velocity to (x,y) of the parent's translation
    parent.Transform2D.TranslateBy(new Vector2(parent.Body.velocityX, parent.Body.velocityY));
  }
  //#endregion

  //#region Common Methods - Equals, ToString, Clone
  Equals(other) {
    //to do...
    throw "Not Yet Implemented";
  }

  ToString() {
    //to do...
    throw "Not Yet Implemented";
  }

  Clone() {
    //to do...
    throw "Not Yet Implemented";
  }
  //#endregion
}*/