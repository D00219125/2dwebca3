//#region AUDIO DATA
//audio - step 2 - create an array with all cues
//note the name we use below MUST be identical to id used in HTML when loading the sound asset
const audioCueArray = [
  new AudioCue("coin_pickup", AudioType.Pickup, 1, 1, false, 0),
  //add more cues here but make sure you load in the HTML!
  new AudioCue("zlime_sound",AudioType.Enemy,1,1,false,0),
  new AudioCue("pistol_pickup",AudioType.Pickup, 1, 1, false, 0),
  new AudioCue("rifle_pickup",AudioType.Pickup, 1, 1, false, 0),
  new AudioCue("flamethrower_pickup",AudioType.Pickup,1,1,false,0),
  new AudioCue("backround_Music",AudioType.Pickup, 1, 1, false, 0)

];
//See Game::LoadAllOtherManagers() for SoundManager instanciation
//#endregion


//#region SPRITE DATA - LEVEL LAYOUT

/*
id:                         descriptive string for the object, does not have to be unique
spriteSheet:                handle to the sprite sheet resource from id specfied in HTML file
sourcePosition:             (x,y) texture space co-ordinates of the top-left corner of the sprite data in the spritesheet
sourceDimensions:           original un-scaled dimensions (w,h) of the sprite data in the spritesheet
rotationInRadians:          rotation angle in radians to be applied to the drawn sprite about its origin
scale:                      scale to be applied to the drawn sprite about its origin
origin:                     (x,y) texture space co-ordinates for the point of rotation/scale for the sprite
actorType:                  actor type (remember the number associated with ActorType determines draw order on screen - see ActorType and RenderManager::Draw()
statusType:                 status type (normally IsDrawn, use IsDrawn | IsUpdated if the sprite has an attached behaviour or animated artist)
scrollSpeedMultiplier:      defines how much the sprite moves in relation to camera movement (1 = move as fast as camera, <1 = move slower than camera)
layerDepth:                 defines the draw order for all sprites of the same ActorType (see RenderManager::Draw())    
alpha:                      opacity of the drawn sprite (0=transparent, 1=opaque)
collisionProperties:        defines if the sprite is collidable (CollidableType), what shape we use (CollisionPrimitiveType) and the appropriate parameter for that shape (e.g. radius vs explodeBy)
*/
const LEVEL_ARCHITECTURE_DATA = Object.freeze({
  //an array of all the sprite objects (i.e. sheet, sourceposition etc) that are used to make the level
  id: "level architecture data",
  levelSprites: {
    1: { //grass
      spriteSheet: document.getElementById("concrete_and_brick"),
      sourcePosition: new Vector2(42, 0),
      sourceDimensions: new Vector2(42, 42),
      rotationInRadians: 0,
      scale: new Vector2(1, 1),
      origin: new Vector2(0, 0),
      actorType: ActorType.Architecture,
      statusType: StatusType.IsDrawn,
      scrollSpeedMultiplier: 1,
      layerDepth: 0,
      alpha: 1,
      collisionProperties: {
        type: CollisionType.Collidable,
        primitive: CollisionPrimitiveType.Rectangle,
        //if circle then set circleRadius, if rectangle then set explodeRectangleBy - but NOT both
        circleRadius: 0,
        explodeRectangleBy: 0,
      }
    },
    2: {  //block
      spriteSheet: document.getElementById("concrete_and_brick"),
      sourcePosition: new Vector2(84, 0),
      sourceDimensions: new Vector2(42, 42),
      rotationInRadians: 0,
      scale: new Vector2(1, 1),
      origin: new Vector2(0, 0),
      actorType: ActorType.Architecture,
      statusType: StatusType.IsDrawn,
      scrollSpeedMultiplier: 1,
      layerDepth: 0,
      alpha: 1,
      collisionProperties: {
        type: CollisionType.Collidable,
        primitive: CollisionPrimitiveType.Rectangle,
        //if circle then set circleRadius, if rectangle then set explodeRectangleBy - but NOT both
        circleRadius: 0,
        explodeRectangleBy: 0,
      }
    },
    3: { //single connector
      spriteSheet: document.getElementById("concrete_and_brick"),
      sourcePosition: new Vector2(126, 0),
      sourceDimensions: new Vector2(42, 42),
      rotationInRadians: 0,
      scale: new Vector2(1, 1),
      origin: new Vector2(0, 0),
      actorType: ActorType.Architecture,
      statusType: StatusType.IsDrawn,
      scrollSpeedMultiplier: 1,
      layerDepth: 0,
      alpha: 1,
      collisionProperties: {
        type: CollisionType.Collidable,
        primitive: CollisionPrimitiveType.Rectangle,
        //if circle then set circleRadius, if rectangle then set explodeRectangleBy - but NOT both
        circleRadius: 0,
        explodeRectangleBy: 0,
      }
    },
    4: { //connector top + grass
      spriteSheet: document.getElementById("concrete_and_brick"),
      sourcePosition: new Vector2(252, 0),
      sourceDimensions: new Vector2(42, 42),
      rotationInRadians: 0,
      scale: new Vector2(1, 1),
      origin: new Vector2(0, 0),
      actorType: ActorType.Architecture,
      statusType: StatusType.IsDrawn,
      scrollSpeedMultiplier: 1,
      layerDepth: 0,
      alpha: 1,
      collisionProperties: {
        type: CollisionType.Collidable,
        primitive: CollisionPrimitiveType.Rectangle,
        //if circle then set circleRadius, if rectangle then set explodeRectangleBy - but NOT both
        circleRadius: 0,
        explodeRectangleBy: 0,
      }
    },
    5: { //connector bottom + grass
      spriteSheet: document.getElementById("concrete_and_brick"),
      sourcePosition: new Vector2(168, 0),
      sourceDimensions: new Vector2(42, 42),
      rotationInRadians: 0,
      scale: new Vector2(1, 1),
      origin: new Vector2(0, 0),
      actorType: ActorType.Architecture,
      statusType: StatusType.IsDrawn,
      scrollSpeedMultiplier: 1,
      layerDepth: 0,
      alpha: 1,
      collisionProperties: {
        type: CollisionType.Collidable,
        primitive: CollisionPrimitiveType.Rectangle,
        //if circle then set circleRadius, if rectangle then set explodeRectangleBy - but NOT both
        circleRadius: 0,
        explodeRectangleBy: 0,
      }
    },
    6: { //connector left + grass
      spriteSheet: document.getElementById("concrete_and_brick"),
      sourcePosition: new Vector2(210, 0),
      sourceDimensions: new Vector2(42, 42),
      rotationInRadians: 0,
      scale: new Vector2(1, 1),
      origin: new Vector2(0, 0),
      actorType: ActorType.Architecture,
      statusType: StatusType.IsDrawn,
      scrollSpeedMultiplier: 1,
      layerDepth: 0,
      alpha: 1,
      collisionProperties: {
        type: CollisionType.Collidable,
        primitive: CollisionPrimitiveType.Rectangle,
        //if circle then set circleRadius, if rectangle then set explodeRectangleBy - but NOT both
        circleRadius: 0,
        explodeRectangleBy: 0,
      }
    },
    7: { //connector right + grass
      spriteSheet: document.getElementById("concrete_and_brick"),
      sourcePosition: new Vector2(294, 0),
      sourceDimensions: new Vector2(42, 42),
      rotationInRadians: 0,
      scale: new Vector2(1, 1),
      origin: new Vector2(0, 0),
      actorType: ActorType.Architecture,
      statusType: StatusType.IsDrawn,
      scrollSpeedMultiplier: 1,
      layerDepth: 0,
      alpha: 1,
      collisionProperties: {
        type: CollisionType.Collidable,
        primitive: CollisionPrimitiveType.Rectangle,
        //if circle then set circleRadius, if rectangle then set explodeRectangleBy - but NOT both
        circleRadius: 0,
        explodeRectangleBy: 0,
      }
    },
    8: { //double connector
      spriteSheet: document.getElementById("concrete_and_brick"),
      sourcePosition: new Vector2(0, 0),
      sourceDimensions: new Vector2(42, 42),
      rotationInRadians: 0,
      scale: new Vector2(1, 1),
      origin: new Vector2(0, 0),
      actorType: ActorType.Decorator,
      statusType: StatusType.IsDrawn,
      scrollSpeedMultiplier: 1,
      layerDepth: 0,
      alpha: 1,
      collisionProperties: {
        type: CollisionType.NotCollidable, //notice this is non-collidable because the player CANNOT reach it
        primitive: CollisionPrimitiveType.Rectangle,
        //if circle then set circleRadius, if rectangle then set explodeRectangleBy - but NOT both
        circleRadius: 0,
        explodeRectangleBy: 0,
      }
    },
    //add more sprite objects to build out the architecture in your level
     9: { //exit
    spriteSheet: document.getElementById("concrete_and_brick"),
    sourcePosition: new Vector2(84, 0),
    sourceDimensions: new Vector2(42, 42),
    rotationInRadians: 0,
    scale: new Vector2(1, 1),
    origin: new Vector2(0, 0),
    actorType: ActorType.Pickup,
    statusType: StatusType.IsDrawn,
    scrollSpeedMultiplier: 1,
    layerDepth: 0,
    alpha: 1,
    collisionProperties: {
      type: CollisionType.Collidable, //notice this is non-collidable because the player CANNOT reach it
      primitive: CollisionPrimitiveType.Rectangle,
      //if circle then set circleRadius, if rectangle then set explodeRectangleBy - but NOT both
      circleRadius: 0,
      explodeRectangleBy: 10,
    }
  }
  //add more sprite objects to build out the architecture in your level
  },
 

  maxBlockWidth: 42,
  maxBlockHeight: 42, 
  /* Why use 33 rows x 20 cols?
    * We can see that our largest sprite block is 42x42 
    * (i.e. "green block") so from this we need 33 rows and 15 columns
    * to cover a total "game area" of 840 x 1386 
    *   20x42 = 840 pixels 
    *   33x42 = 1386 pixels 
    * This means each player can see ALL the width of the level but 
    * only a portion (i.e. canvas height) of the height.
    */
  levelLayoutArray: [
    [1,1,1,1,5,5,5,5,1,1,1,1,5,5,5,5,1,1,1,1],
    [1,0,0,0,0,0,0,0,9,0,0,0,0,0,0,0,0,0,0,1],
    [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
    [4,0,0,1,3,1,0,0,1,3,3,1,0,0,1,3,1,0,0,6],
    [4,0,0,1,5,1,0,0,1,8,8,1,0,0,1,5,1,0,0,6],
    [1,0,0,0,0,0,0,0,1,8,8,1,0,0,0,0,0,0,0,1], 
    [1,0,0,0,0,0,0,0,1,5,5,1,0,0,0,0,0,0,0,1],
    [1,3,3,3,3,1,0,0,0,0,0,0,0,0,1,3,3,3,3,1], 
    [1,5,5,5,5,1,0,0,0,0,0,0,0,0,1,5,5,5,5,1], 
    [1,0,0,0,0,0,0,0,1,3,3,1,0,0,0,0,0,0,0,1],
    [1,0,0,0,0,0,0,0,1,8,8,1,0,0,0,0,0,0,0,1],
    [4,0,0,1,7,7,7,7,1,5,5,1,7,7,7,7,1,0,0,6], 
    [4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6],
    [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
    [8,3,3,3,1,0,0,1,3,3,3,3,1,1,1,1,3,3,3,8],
    [8,8,8,8,1,0,0,1,8,8,8,8,8,8,8,8,8,8,8,8], 
    [8,5,5,5,1,0,0,1,5,5,5,5,1,1,1,1,5,5,5,8],
    [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],  
    [4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6], 
    [4,0,0,1,7,7,7,7,1,3,3,1,7,7,7,7,1,0,0,6],
    [1,0,0,0,0,0,0,0,1,8,8,1,0,0,0,0,0,0,0,1],
    [1,0,0,0,0,0,0,0,1,5,5,1,0,0,0,0,0,0,0,1],
    [1,3,3,3,3,3,3,1,0,0,0,0,0,0,1,3,3,3,3,1],
    [1,5,5,5,5,5,5,1,0,0,0,0,0,0,1,5,5,5,5,1], 
    [1,0,0,0,0,0,0,0,1,3,3,1,0,0,0,0,0,0,0,1],
    [1,0,0,0,0,0,0,0,1,8,8,1,0,0,0,0,0,0,0,1],  
    [4,0,0,0,3,1,0,0,1,8,8,1,0,0,1,3,1,0,0,6],
    [4,0,0,1,8,1,0,0,1,5,5,1,0,0,1,8,1,0,0,6],
    [4,0,0,1,8,1,0,0,0,0,0,0,0,0,1,8,1,0,0,6],
    [4,0,0,1,5,1,0,0,0,0,0,0,0,0,1,5,1,0,0,6], 
    [1,0,0,0,0,0,0,0,1,3,3,1,0,0,0,0,0,0,0,1],
    [1,0,0,0,0,0,0,0,1,8,8,1,0,0,0,0,0,0,0,1],
    [1,1,1,1,3,3,3,3,1,1,1,1,3,3,3,3,1,1,1,1]
    
  ]
});

/*
id:                         descriptive string for the object, does not have to be unique
spriteSheet:                handle to the sprite sheet resource from id specfied in HTML file
sourcePosition:             (x,y) texture space co-ordinates of the top-left corner of the sprite data in the spritesheet
sourceDimensions:           original un-scaled dimensions (w,h) of the sprite data in the spritesheet
rotationInRadians:          rotation angle in radians to be applied to the drawn sprite about its origin
scale:                      scale to be applied to the drawn sprite about its origin
origin:                     (x,y) texture space co-ordinates for the point of rotation/scale for the sprite
actorType:                  actor type (remember the number associated with ActorType determines draw order on screen - see ActorType and RenderManager::Draw()
statusType:                 status type (normally IsDrawn, use IsDrawn | IsUpdated if the sprite has an attached behaviour or animated artist)
scrollSpeedMultiplier:      defines how much the sprite moves in relation to camera movement (1 = move as fast as camera, <1 = move slower than camera)
layerDepth:                 defines the draw order for all sprites of the same ActorType (see RenderManager::Draw())    
alpha:                      opacity of the drawn sprite (0=transparent, 1=opaque)
collisionProperties:        defines if the sprite is collidable (CollidableType), what shape we use (CollisionPrimitiveType) and the appropriate parameter for that shape (e.g. radius vs explodeBy)
*/
const LEVEL_PICKUPS_DATA = Object.freeze({
  //an array of all the sprite objects (i.e. sheet, sourceposition etc) that are used to make the level
  id: "level pickups data",
  levelSprites: {
    1: { //RIFLE
      spriteSheet: document.getElementById("weapons_and_pickups"),
      sourcePosition: new Vector2(0, 0),
      sourceDimensions: new Vector2(495, 110),
      rotationInRadians: 0,
      scale: new Vector2(.1, .1),
      origin: new Vector2(21, 21),
      actorType: ActorType.Pickup,
      statusType: StatusType.IsDrawn,
      scrollSpeedMultiplier: 1,
      layerDepth: 0,
      alpha: 1,
      collisionProperties: {
        type: CollisionType.Collidable,
        primitive: CollisionPrimitiveType.Rectangle,
        //if circle then set circleRadius, if rectangle then set explodeRectangleBy - but NOT both
        circleRadius: 0,
        explodeRectangleBy: 10,
      },
      //adding this as unique id
      idForSwitching:"rifle"
    },
    2: { //flamethrower
      spriteSheet: document.getElementById("weapons_and_pickups"),
      sourcePosition: new Vector2(495, 0),
      sourceDimensions: new Vector2(432, 120),
      rotationInRadians: 0,
      scale: new Vector2(.1, .1),
      origin: new Vector2(21, 21),
      actorType: ActorType.Pickup,
      statusType: StatusType.IsDrawn,
      scrollSpeedMultiplier: 1,
      layerDepth: 0,
      alpha: 1,
      collisionProperties: {
        type: CollisionType.Collidable,
        primitive: CollisionPrimitiveType.Rectangle,
        //if circle then set circleRadius, if rectangle then set explodeRectangleBy - but NOT both
        circleRadius: 0,
        explodeRectangleBy: 10,
      },
      //adding this as unique id
      idForSwitching:"flamethrower"
    },
    3: { //pistol
      spriteSheet: document.getElementById("weapons_and_pickups"),
      sourcePosition: new Vector2(927, 0),
      sourceDimensions: new Vector2(235, 120),
      rotationInRadians: 0,
      scale: new Vector2(.1, .1),
      origin: new Vector2(21, 21),
      actorType: ActorType.Pickup,
      statusType: StatusType.IsDrawn,
      scrollSpeedMultiplier: 1,
      layerDepth: 0,
      alpha: 1,
      collisionProperties: {
        type: CollisionType.Collidable,
        primitive: CollisionPrimitiveType.Rectangle,
        //if circle then set circleRadius, if rectangle then set explodeRectangleBy - but NOT both
        circleRadius: 0,
        explodeRectangleBy: 10,
      },
      //adding this as unique id
      idForSwitching:"pistol"
    },
    4: { //Health pack
      spriteSheet: document.getElementById("weapons_and_pickups"),
      sourcePosition: new Vector2(1164, 0),
      sourceDimensions: new Vector2(135, 133),
      rotationInRadians: 0,
      scale: new Vector2(.1, .1),
      origin: new Vector2(21, 21),
      actorType: ActorType.Pickup,
      statusType: StatusType.IsDrawn,
      scrollSpeedMultiplier: 1,
      layerDepth: 0,
      alpha: 1,
      collisionProperties: {
        type: CollisionType.Collidable,
        primitive: CollisionPrimitiveType.Rectangle,
        //if circle then set circleRadius, if rectangle then set explodeRectangleBy - but NOT both
        circleRadius: 0,
        explodeRectangleBy: 10,
      }
    }
    //add more sprite objects to build out the architecture in your level
  },
  maxBlockWidth: 42,
  maxBlockHeight: 42, 
  levelLayoutArray: [
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0], 
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0], 
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0], 
    [0,1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0], 
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0], 
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],  
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0], 
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0], 
    [0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],  
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0], 
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
  ]
});
//#endregion

//#region SPRITE DATA - ANIMATED PLAYERS
/*
id:                         descriptive string for the object, does not have to be unique
spriteSheet:                handle to the sprite sheet resource from id specfied in HTML file
defaultTakeName:            string name of the take to play when the animation is loaded
translation:                translation used to position the object on the screen
rotationInRadians:          rotation angle in radians to be applied to the drawn sprite about its origin
scale:                      scale to be applied to the drawn sprite about its origin
origin:                     (x,y) texture space co-ordinates for the point of rotation/scale for the sprite
actorType:                  actor type (remember the number associated with ActorType determines draw order on screen - see ActorType and RenderManager::Draw()
statusType:                 status type (normally IsDrawn, use IsDrawn | IsUpdated if the sprite has an attached behaviour or animated artist)
scrollSpeedMultiplier:      defines how much the sprite moves in relation to camera movement (1 = move as fast as camera, <1 = move slower than camera)
layerDepth:                 defines the draw order for all sprites of the same ActorType (see RenderManager::Draw())    
alpha:                      opacity of the drawn sprite (0=transparent, 1=opaque)
collisionProperties:        defines if the sprite is collidable (CollidableType), what shape we use (CollisionPrimitiveType) and the appropriate parameter for that shape (e.g. radius vs explodeBy)
moveProperties:             defines fields related to movement of the sprite (e.g. initial look direction, move speed, rotate speed, gravity, friction, max speed)
takes:                      a compound object (takes) containing a set of key-value pairs representing the take name (e.g. walk) and all the data related to that take (e.g. fps, start frame, end frame, bounding box, an array of rectangles indicating where the sprites are in the source sprite sheet)
*/

const PLAYER_ONE_DATA = Object.freeze({
  id: "player 1",
  spriteSheet: document.getElementById("man_animations"),
  defaultTakeName: "walk_with_bat",
  translation: new Vector2(275, 1280),
  rotationInRadians: GDMath.ToRadians(180),
  scale: new Vector2(.1, .1),
  origin: new Vector2(260, 250),
  actorType: ActorType.Player,
  statusType: StatusType.IsDrawn | StatusType.IsUpdated,
  scrollSpeedMultiplier: 1,
  layerDepth: 1,
  alpha: 1,
  //added health in here
  health: 5,
  collisionProperties: {
    type: CollisionType.Collidable,
    primitive: CollisionPrimitiveType.Circle,
    //if circle then set circleRadius, if rectangle then set explodeRectangleBy - but NOT both
    circleRadius: 20,
    explodeRectangleBy: 0,
  },
  moveProperties: {
    lookDirection: Vector2.Normalize(new Vector2(0, 1)), //straight-down according to source image
    moveKeys: [Keys.W, Keys.S, Keys.A, Keys.D],
    moveSpeed: 0.05,
    rotateSpeedInRadians: GDMath.ToRadians(0.25),
    gravityType: GravityType.Off, //top-down so no gravity
    frictionType: FrictionType.Normal, 
    maximumSpeed: 4,
    
  },
  takes: {  
    "walk_with_bat" :  {
      fps: 5,
      maxLoopCount: -1, //0 >= always, 1 = run once, N = run N times
      startCellIndex: 0,
      endCellIndex: 5,
      boundingBoxDimensions: new Vector2(100, 100), 
      cellData: [
        new Rect(0, 0, 500, 500),
        new Rect(500, 0, 500, 500),
        new Rect(1000, 0, 500, 500),
        new Rect(1500, 0, 500, 500),
        new Rect(2000, 0, 500, 500),
        new Rect(2500, 0, 500, 500),
      ]
    },
    "idle_with_bat" :  {
      fps: 1,
      maxLoopCount: -1, //0 >= always, 1 = run once, N = run N times
      startCellIndex: 0,
      endCellIndex: 0,
      boundingBoxDimensions: new Vector2(100, 100), 
      cellData: [
        new Rect(1500, 0, 500, 500) //play frame where player stands repeatedly
      ]
    },
    "walk_with_flamethrower" :  {
      fps: 5,
      maxLoopCount: -1, //0 >= always, 1 = run once, N = run N times
      startCellIndex: 0,
      endCellIndex: 5,
      boundingBoxDimensions: new Vector2(100, 100), 
      cellData: [
        new Rect(3020, 0, 360, 530),
        new Rect(3380, 0, 360, 530),
        new Rect(3730, 0, 360, 530),
        new Rect(4085, 0, 360, 530),
        new Rect(4440, 0, 360, 530),
        new Rect(4795, 0, 360, 530),
      ]
    },
    "idle_with_flamethrower" :  {
      fps: 1,
      maxLoopCount: -1, //0 >= always, 1 = run once, N = run N times
      startCellIndex: 0,
      endCellIndex: 0,
      boundingBoxDimensions: new Vector2(100, 100), 
      cellData: [
        new Rect(4795, 0, 360, 530), //play frame where player stands repeatedly
      ]
    },
    "walk_with_pistol" :  {
      fps: 5,
      maxLoopCount: -1, //0 >= always, 1 = run once, N = run N times
      startCellIndex: 0,
      endCellIndex: 5,
      boundingBoxDimensions: new Vector2(100, 100), 
      cellData: [
        new Rect(5145, 0, 330, 415),
        new Rect(5480, 0, 330, 415),
        new Rect(5810, 0, 330, 451),
        new Rect(6145, 0, 330, 415),
        new Rect(6475, 0, 330, 415),
        new Rect(6808, 0, 330, 415),
      ]
    },
    "idle_with_pistol" :  {
      fps: 1,
      maxLoopCount: -1, //0 >= always, 1 = run once, N = run N times
      startCellIndex: 0,
      endCellIndex: 0,
      boundingBoxDimensions: new Vector2(100, 100), 
      cellData: [
        new Rect(6808, 0, 330, 415), //play frame where player stands repeatedly
      ]
    },
    "walk_with_rifle" :  {
      fps: 5,
      maxLoopCount: -1, //0 >= always, 1 = run once, N = run N times
      startCellIndex: 0,
      endCellIndex: 5,
      boundingBoxDimensions: new Vector2(100, 100), 
      cellData: [
        new Rect(10390, 0, 356, 590),
        new Rect(10745, 0, 356, 590),
        new Rect(11100, 0, 356, 590),
        new Rect(11455, 0, 356, 590),
        new Rect(11805, 0, 356, 590),
        new Rect(12165, 0, 356, 590),
      ]
    },
    "idle_with_rifle" :  {
      fps: 1,
      maxLoopCount: -1, //0 >= always, 1 = run once, N = run N times
      startCellIndex: 0,
      endCellIndex: 0,
      boundingBoxDimensions: new Vector2(100, 100), 
      cellData: [
        new Rect(12165, 0, 356, 590), //play frame where player stands repeatedly
      ]
    },
    "attack_with_bat" :  {
      fps: 10,
      maxLoopCount: 1, //0 >= always, 1 = run once, N = run N times
      startCellIndex: 0,
      endCellIndex: 10,
      boundingBoxDimensions: new Vector2(100, 100), 
      cellData: [
        new Rect(10390, 0, 356, 590),
        new Rect(10745, 0, 356, 590),
        new Rect(11100, 0, 356, 590),
        new Rect(11455, 0, 356, 590),
        new Rect(11805, 0, 356, 590),
        new Rect(12165, 0, 356, 590),
        new Rect(10390, 0, 356, 590),
        new Rect(10745, 0, 356, 590),
        new Rect(11100, 0, 356, 590),
        new Rect(11455, 0, 356, 590),
        new Rect(11805, 0, 356, 590),
      ]
    },
  }
});

/*
id:                         descriptive string for the object, does not have to be unique
spriteSheet:                handle to the sprite sheet resource from id specfied in HTML file
defaultTakeName:            string name of the take to play when the animation is loaded
translation:                translation used to position the object on the screen
rotationInRadians:          rotation angle in radians to be applied to the drawn sprite about its origin
scale:                      scale to be applied to the drawn sprite about its origin
origin:                     (x,y) texture space co-ordinates for the point of rotation/scale for the sprite
actorType:                  actor type (remember the number associated with ActorType determines draw order on screen - see ActorType and RenderManager::Draw()
statusType:                 status type (normally IsDrawn, use IsDrawn | IsUpdated if the sprite has an attached behaviour or animated artist)
scrollSpeedMultiplier:      defines how much the sprite moves in relation to camera movement (1 = move as fast as camera, <1 = move slower than camera)
layerDepth:                 defines the draw order for all sprites of the same ActorType (see RenderManager::Draw())    
alpha:                      opacity of the drawn sprite (0=transparent, 1=opaque)
collisionProperties:        defines if the sprite is collidable (CollidableType), what shape we use (CollisionPrimitiveType) and the appropriate parameter for that shape (e.g. radius vs explodeBy)
moveProperties:             defines fields related to movement of the sprite (e.g. initial look direction, move speed, rotate speed, gravity, friction, max speed)
takes:                      a compound object (takes) containing a set of key-value pairs representing the take name (e.g. walk) and all the data related to that take (e.g. fps, start frame, end frame, bounding box, an array of rectangles indicating where the sprites are in the source sprite sheet)
*/
const PLAYER_TWO_DATA = Object.freeze({
  id: "player 2",
  spriteSheet: document.getElementById("woman_animations"),
  defaultTakeName: "walk_with_bat",
  translation: new Vector2(275, 1180),
  rotationInRadians: GDMath.ToRadians(0),
  scale: new Vector2(0.1, 0.1),
  origin: new Vector2(180, 205),
  actorType: ActorType.Player,
  statusType: StatusType.IsDrawn | StatusType.IsUpdated,
  scrollSpeedMultiplier: 1,
  layerDepth: 0,
  alpha: 1,
  //added health in here
  health: 5,
  collisionProperties: {
    type: CollisionType.Collidable,
    primitive: CollisionPrimitiveType.Circle,
    //if circle then set circleRadius, if rectangle then set explodeRectangleBy - but NOT both
    circleRadius: 20,
    explodeRectangleBy: 0,
  },
  moveProperties: {
    lookDirection: Vector2.Normalize(new Vector2(0, 1)),  //straight-down according to source image
    moveKeys: [Keys.I, Keys.K, Keys.J, Keys.L],
    moveSpeed: 0.05,                                                                              //change this back to .05
    rotateSpeedInRadians: GDMath.ToRadians(0.25),
    gravityType: GravityType.Off, //top-down so no gravity
    frictionType: FrictionType.Normal, 
    maximumSpeed: 4,
  },
  takes: {  
    "walk_with_bat" :  {
      fps: 5,
      maxLoopCount: -1, //0 >= always, 1 = run once, N = run N times
      startCellIndex: 0,
      endCellIndex: 5,
      boundingBoxDimensions: new Vector2(100, 100), 
      cellData: [
        new Rect(15, 0, 360, 410),
        new Rect(410, 0, 380, 410),
        new Rect(870, 0, 305,360),
        new Rect(1230, 0, 390, 410),
        new Rect(1635, 0, 390, 410),
        new Rect(2090, 0, 310, 370),
      ]
    },
    "idle_with_bat" :  {
      fps: 1,
      maxLoopCount: -1, //0 >= always, 1 = run once, N = run N times
      startCellIndex: 0,
      endCellIndex: 0,
      boundingBoxDimensions: new Vector2(100, 100), 
      cellData: [
        new Rect(870, 0, 305,360), //play frame where player stands repeatedly
      ]
    },
    "walk_with_flamethrower" :  {
      fps: 5,
      maxLoopCount: -1, //0 >= always, 1 = run once, N = run N times
      startCellIndex: 0,
      endCellIndex: 5,
      boundingBoxDimensions: new Vector2(100, 100), 
      cellData: [
        new Rect(2440, 0, 290, 415),
        new Rect(2730, 0, 285, 415),
        new Rect(3015, 0, 290,400),
        new Rect(3300, 0, 290, 430),
        new Rect(3590, 0, 290, 410),
        new Rect(3877, 0, 290, 420),
      ]
    },
    "idle_with_flamethrower" :  {
      fps: 1,
      maxLoopCount: -1, //0 >= always, 1 = run once, N = run N times
      startCellIndex: 0,
      endCellIndex: 0,
      boundingBoxDimensions: new Vector2(100, 100), 
      cellData: [
        new Rect(3015, 0, 290,400), //play frame where player stands repeatedly
      ]
    },
    "walk_with_pistol" :  {
      fps: 5,
      maxLoopCount: -1, //0 >= always, 1 = run once, N = run N times
      startCellIndex: 0,
      endCellIndex: 5,
      boundingBoxDimensions: new Vector2(100, 100), 
      cellData: [
        new Rect(4165, 0, 270, 350),
        new Rect(4435, 0, 270, 350),
        new Rect(4705, 0, 275,350),
        new Rect(4975, 0, 275, 350),
        new Rect(5249, 0, 275, 350),
        new Rect(5520, 0, 270, 340),
      ]
    },
    "idle_with_pistol" :  {
      fps: 1,
      maxLoopCount: -1, //0 >= always, 1 = run once, N = run N times
      startCellIndex: 0,
      endCellIndex: 0,
      boundingBoxDimensions: new Vector2(100, 100), 
      cellData: [
        new Rect(4705, 0, 275,350), //play frame where player stands repeatedly
      ]
    },
    "walk_with_rifle" :  {
      fps: 5,
      maxLoopCount: -1, //0 >= always, 1 = run once, N = run N times
      startCellIndex: 0,
      endCellIndex: 5,
      boundingBoxDimensions: new Vector2(100, 100), 
      cellData: [
        new Rect(5790, 0, 288, 500),
        new Rect(6075, 0, 288, 500),
        new Rect(6363, 0, 288,510),
        new Rect(6650, 0, 290, 480),
        new Rect(6937, 0, 290, 490),
        new Rect(7225, 0, 288, 510),
      ]
    },
    "idle_with_rifle" :  {
      fps: 1,
      maxLoopCount: -1, //0 >= always, 1 = run once, N = run N times
      startCellIndex: 0,
      endCellIndex: 0,
      boundingBoxDimensions: new Vector2(100, 100), 
      cellData: [
        new Rect(6650, 0, 290, 480), //play frame where player stands repeatedly
      ]
    },

  }
});
//#endregion

//#region SPRITE DATA - ANIMATED PICKUP
/*
id:                         descriptive string for the object, does not have to be unique
spriteSheet:                handle to the sprite sheet resource from id specfied in HTML file
defaultTakeName:            string name of the take to play when the animation is loaded
translation:                translation used to position the object on the screen
rotationInRadians:          rotation angle in radians to be applied to the drawn sprite about its origin
scale:                      scale to be applied to the drawn sprite about its origin
origin:                     (x,y) texture space co-ordinates for the point of rotation/scale for the sprite
actorType:                  actor type (remember the number associated with ActorType determines draw order on screen - see ActorType and RenderManager::Draw()
statusType:                 status type (normally IsDrawn, use IsDrawn | IsUpdated if the sprite has an attached behaviour or animated artist)
scrollSpeedMultiplier:      defines how much the sprite moves in relation to camera movement (1 = move as fast as camera, <1 = move slower than camera)
layerDepth:                 defines the draw order for all sprites of the same ActorType (see RenderManager::Draw())    
alpha:                      opacity of the drawn sprite (0=transparent, 1=opaque)
collisionProperties:        defines if the sprite is collidable (CollidableType), what shape we use (CollisionPrimitiveType) and the appropriate parameter for that shape (e.g. radius vs explodeBy)
moveProperties:             defines fields related to movement of the sprite (e.g. initial look direction, move speed, rotate speed, gravity, friction, max speed), a null indicates that this animation doesnt move
takes:                      a compound object (takes) containing a set of key-value pairs representing the take name (e.g. walk) and all the data related to that take (e.g. fps, start frame, end frame, bounding box, an array of rectangles indicating where the sprites are in the source sprite sheet)
*/
const PICKUP_COIN_ANIMATION_DATA = Object.freeze({
  id: "zlime",
  spriteSheet: document.getElementById("zlime"),
  defaultTakeName: 1,
  translation: new Vector2(510, 126),
  rotationInRadians: 0,
  scale: new Vector2(0.2,0.2),
  origin: new Vector2(70, 60),
  actorType: ActorType.Pickup,
  statusType: StatusType.IsDrawn | StatusType.IsUpdated,
  scrollSpeedMultiplier: 1,
  layerDepth: 0,
  alpha: 1,
  collisionProperties: {
    type: CollisionType.Collidable,
    primitive: CollisionPrimitiveType.Circle,
    //if circle then set circleRadius, if rectangle then set explodeRectangleBy - but NOT both
    circleRadius: 12,
    explodeRectangleBy: 0,
  },
  moveProperties: null, //null means this animation doesnt move (e.g. like a static coin pickup)
  takes: {  
    1 :  {
      fps: 6,
      maxLoopCount: -1, //0 >= always, 1 = run once, N = run N times
      startCellIndex: 0,
      endCellIndex: 2,
      boundingBoxDimensions: new Vector2(199, 170), 
      cellData: [
        new Rect(76, 6, 157, 120),
        new Rect(472, 5, 158, 124),
        new Rect(277, 8, 162, 123)
      ]
    }
  }
});

//#endregion

//#region SPRITE DATA - ANIMATION DECORATORS (fire, pickup)
const PICKUP_COIN_DECORATOR_ANIMATION_DATA = Object.freeze({
  id: "coin_pickup_decorator",
  spriteSheet: document.getElementById("pickup_collision_animation"),
  defaultTakeName: "explode",
  translation: new Vector2(200, 200),
  rotationInRadians: 0,
  scale: new Vector2(0.5, 0.3),
  origin: new Vector2(0, 0),
  actorType: ActorType.NonCollidableAnimatedDecorator,
  statusType: StatusType.Off,
  scrollSpeedMultiplier: 1,
  layerDepth: 0,
  alpha: 1,
  collisionProperties: {
    type: CollisionType.NotCollidable,
    primitive: CollisionPrimitiveType.None,
    //if circle then set circleRadius, if rectangle then set explodeRectangleBy - but NOT both
    circleRadius: 0,
    explodeRectangleBy: 0,
  },
  moveProperties: null, //null means this animation doesnt move (e.g. like a static coin pickup)
  takes: {  
    "explode" :  {
      fps: 20,
      maxLoopCount:   1, //0 >= always, 1 = run once, N = run N times
      startCellIndex: 0,
      endCellIndex: 8,
      boundingBoxDimensions: new Vector2(157, 157), 
      cellData: [
        new Rect(0, 0, 157, 157),
        new Rect(157, 0, 157, 157),
        new Rect(314, 0, 157, 157),
        new Rect(471, 0, 157, 157),
        new Rect(628, 0, 157, 157),
        new Rect(785, 0, 157, 157),
        new Rect(942, 0, 157, 157),
        new Rect(1099, 0, 157, 157),
        new Rect(1256, 0, 157, 157)
      ]
    }
  }
});
//complete object to load the animation for pickup of coins...
//#endregion

//#region SPRITE DATA - ANIMATION DECORATORS (fire, pickup)
const TOAST_GET_READY_ANIMATION_DATA = Object.freeze({
  id: "get_ready_animation",
  spriteSheet: document.getElementById("get_ready_animation"),
  defaultTakeName: "ready",
  translation: new Vector2(200, 200),
  rotationInRadians: 0,
  scale: new Vector2(0.2, 0.2),
  origin: new Vector2(0, 0),
  actorType: ActorType.NonCollidableAnimatedDecorator,
  statusType: StatusType.Off,
  scrollSpeedMultiplier: 1,
  layerDepth: 0,
  alpha: 1,
  collisionProperties: {
    type: CollisionType.NotCollidable,
    primitive: CollisionPrimitiveType.None,
    //if circle then set circleRadius, if rectangle then set explodeRectangleBy - but NOT both
    circleRadius: 0,
    explodeRectangleBy: 0,
  },
  moveProperties: null, //null means this animation doesnt move (e.g. like a static coin pickup)
  takes: {  
    "ready" :  {
      fps: 20,
      maxLoopCount:   1, //0 >= always, 1 = run once, N = run N times
      startCellIndex: 0,
      endCellIndex: 3,
      boundingBoxDimensions: new Vector2(1378, 309), 
      cellData: [
        new Rect(0, 0, 1378, 309), 
        new Rect(1378, 0, 1378, 309), 
        new Rect(2756, 0, 1378, 309), 
        new Rect(4134, 0, 1378, 309), 
        //add more frames from the Get Ready animation here...
      ]
    }
  }
});
/*
//complete object to load the animation for pickup of coins...
const SHOOTING_ANIMATION_DATA = Object.freeze({
  id: "bullet",
  spriteSheet: document.getElementById("Bullet"),
  defaultTakeName: "pistol_shot",
  translation: new Vector2(200, 200),
  rotationInRadians: 0,
  scale: new Vector2(0.3, 0.3),
  origin: new Vector2(0, 0),
  actorType: ActorType.NonCollidableAnimatedDecorator,
  statusType: StatusType.Off,
  scrollSpeedMultiplier: 1,
  layerDepth: 0,
  alpha: 1,
  collisionProperties: {
    type: CollisionType.Collidable,
    primitive: CollisionPrimitiveType.Rectangle,
    //if circle then set circleRadius, if rectangle then set explodeRectangleBy - but NOT both
    circleRadius: 0,
    explodeRectangleBy: 10,
  },
  moveProperties: null, //null means this animation doesnt move (e.g. like a static coin pickup)
  takes: {  
    "pistol_shot" :  {
      fps: 20,
      maxLoopCount:   1, //0 >= always, 1 = run once, N = run N times
      startCellIndex: 0,
      endCellIndex: 3,
      boundingBoxDimensions: new Vector2(18, 35), 
      cellData: [
        new Rect(291, 106, 36, 51), 
        //add more frames from the Get Ready animation here...
      ]
    }
  }
});

const LEVEL_ENEMIES_DATA = Object.freeze({
  //an array of all the sprite objects (i.e. sheet, sourceposition etc) that are used to make the level
  id: "zlime",
  levelSprites: {
    1: { //zlime
      spriteSheet: document.getElementById("zlime"),
      sourcePosition: new Vector2(76,8),
      sourceDimensions: new Vector2(157, 122),
      rotationInRadians: 0,
      scale: new Vector2(.1, .1),
      origin: new Vector2(75, 61),
      actorType: ActorType.Enemy,
      statusType: StatusType.IsDrawn,
      scrollSpeedMultiplier: 1,
      layerDepth: 0,
      alpha: 1,
      collisionProperties: {
        type: CollisionType.Collidable,
        primitive: CollisionPrimitiveType.Rectangle,
        //if circle then set circleRadius, if rectangle then set explodeRectangleBy - but NOT both
        circleRadius: 0,
        explodeRectangleBy: 10,
      }
    },
    //add more sprite objects to build out the architecture in your level
  },
  maxBlockWidth: 42,
  maxBlockHeight: 42, 
  levelLayoutArray: [
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,1,2,2,2,2,2,3,2,2,2,3,2,2,2,4,2,2,1,0],
    [0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0],
    [0,2,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,2,0],
    [0,2,0,0,0,0,0,2,0,0,0,0,2,0,0,0,0,0,2,0],
    [0,2,0,0,0,0,0,2,0,0,0,0,2,0,0,0,0,0,2,0], 
    [0,1,2,2,2,2,2,2,0,0,0,0,2,2,2,2,2,2,1,0],
    [0,0,0,0,0,0,2,2,2,2,2,2,2,0,0,0,0,0,0,0], 
    [0,0,0,0,0,0,2,0,0,0,0,0,2,0,0,0,0,0,0,0], 
    [0,1,2,2,2,2,2,0,0,0,0,0,2,0,0,0,0,0,0,0],
    [0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0], 
    [0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,1,2,2,2,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0], 
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],  
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0], 
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0],
    [0,0,0,0,0,0,2,0,0,0,0,0,0,2,0,0,0,0,0,0],
    [0,0,0,0,0,0,2,0,0,0,0,0,0,2,0,0,0,0,0,0], 
    [0,1,2,2,2,2,2,2,0,0,0,0,2,2,2,2,2,2,1,0],
    [0,2,0,0,0,0,0,2,0,0,0,0,2,0,0,0,0,0,2,0],  
    [0,2,0,0,0,0,0,2,0,0,0,0,2,0,0,0,0,0,2,0],
    [0,2,0,0,0,0,0,2,0,0,0,0,2,0,0,0,0,0,2,0],
    [0,2,0,0,0,0,0,2,0,0,0,0,2,0,0,0,0,0,2,0],
    [0,2,0,0,0,0,0,2,2,2,2,2,2,0,0,0,0,0,2,0], 
    [0,2,0,0,0,0,0,2,0,0,0,0,2,0,0,0,0,0,2,0],
    [0,1,2,2,2,2,2,2,0,0,0,0,1,2,2,2,4,2,1,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
  ]
});*/
const LEVEL_Zlimes_DATA = Object.freeze({
  //an array of all the sprite objects (i.e. sheet, sourceposition etc) that are used to make the level
  id: "level pickups data",
  levelSprites: {
    1: { //Slime
      spriteSheet: document.getElementById("zlime"),
      sourcePosition: new Vector2(0, 0),
      sourceDimensions: new Vector2(510, 126),
      rotationInRadians: 0,
      scale: new Vector2(.2, .2),
      origin: new Vector2(70, 60),
      actorType: ActorType.Enemy,
      statusType: StatusType.IsDrawn,
      scrollSpeedMultiplier: 1,
      layerDepth: 0,
      alpha: 1,
      collisionProperties: {
        type: CollisionType.Collidable,
        primitive: CollisionPrimitiveType.Rectangle,
        //if circle then set circleRadius, if rectangle then set explodeRectangleBy - but NOT both
        circleRadius: 0,
        explodeRectangleBy: 10,
      }
    }
    //add more sprite objects to build out the architecture in your level
  },
  maxBlockWidth: 42,
  maxBlockHeight: 42, 
  levelLayoutArray: [
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0], 
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0], 
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0], 
    [0,1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0], 
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0], 
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],  
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0], 
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0], 
    [0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],  
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0], 
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
  ]
});
//#endregion


